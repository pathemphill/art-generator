﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace ArtGenerator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> _characters;
        private List<string> _objects;

        private const string characterFile = "characters.txt";
        private const string objectsFile = "objects.txt";
        private Random _random;

        public MainWindow()
        {
            InitializeComponent();
            _random = new Random();
            _characters = new List<string>();
            _objects = new List<string>();
            if (!loadCharacters())
            {
                _characters.Add("Elvira");
                _characters.Add("Wolverine");
                _characters.Add("Mega Man");
                _characters.Add("Katt");
            }
            if (!loadObjects())
            { 
                _objects.Add("Garden Hose");
                _objects.Add("Bikini");
                _objects.Add("T-square");
                _objects.Add("Hammer");
            }
            updateCombinationCount();
            generateCombination();
        }

        private bool loadCharacters()
        {
            if (System.IO.File.Exists(characterFile)) 
            {
                StreamReader reader = new StreamReader(characterFile);
                while(!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    _characters.Add(line);
                }
                return true;
            }
            return false;
        }

        private bool loadObjects()
        {
            if (System.IO.File.Exists(objectsFile))
            {
                StreamReader reader = new StreamReader(objectsFile);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    _objects.Add(line);
                }
                return true;
            }
            return false;
        }

        private void generateCombination()
        {
            int newChar = _random.Next(_characters.Count);
            System.Diagnostics.Debug.Print("new character: " + newChar);
            lblCharacter.Content = _characters.ElementAt(newChar);
            int newObj = _random.Next(_objects.Count);
            System.Diagnostics.Debug.Print("new object: " + newObj);
            lblObject.Content = _objects.ElementAt(newObj);
        }

        private void updateCombinationCount()
        {
            lblCombinations.Content = _characters.Count * _objects.Count + " Possible Combinations";
        }

        private void generate_click(object sender, RoutedEventArgs args)
        {
            generateCombination();
        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_characters != null && _characters.Count > 0)
            {
                try
                {
                    StreamWriter characterWriter = new StreamWriter(characterFile, false);
                    foreach (string character in _characters)
                    {
                        characterWriter.WriteLine(character);
                    }
                    characterWriter.Close();
                } catch (IOException ioe)
                {
                    System.Diagnostics.Debug.Print(ioe.Message + ioe.StackTrace);
                    //do nothing... this isn't that important
                }
            }
            if (_objects != null && _objects.Count > 0)
            {
                try {
                    StreamWriter objectWriter = new StreamWriter(objectsFile);
                    foreach (string current in _objects)
                    {
                        objectWriter.WriteLine(current);
                    }
                    objectWriter.Close();
                } catch (IOException ioe)
                {
                    System.Diagnostics.Debug.Print(ioe.Message + ioe.StackTrace);
                    //do nothing... this isn't that important
                }
            }
        }

        private void btnNewCharacter_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewCharacter.Text) && !_characters.Contains(txtNewCharacter.Text))
            {
                _characters.Add(txtNewCharacter.Text);
                updateCombinationCount();
            }
        }

        private void btnNewObject_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewObject.Text) && !_objects.Contains(txtNewObject.Text))
            {
                _objects.Add(txtNewObject.Text);
                updateCombinationCount();
            }
        }
    }
}
